
var gulp = require('gulp');
var sass = require('gulp-ruby-sass');
var webserver = require('gulp-webserver');
var jade = require('gulp-jade');
var autoprefixer = require('gulp-autoprefixer');

gulp.task('webserver', function(){
		gulp.src('dist')
			.pipe(webserver({
							livereload: true,
								port: 8039,
								open: true
						}));
});

gulp.task('html', function() {
		  gulp.src('./app/*.jade')
				.pipe(jade({ }))
			.pipe(gulp.dest('./dist/'))

});


gulp.task('css', () =>
    sass('./app/sass/*.scss')
        .on('error', sass.logError)
				.pipe(autoprefixer({
				   browsers: ['last 2 versions'],
				  cascade: false
				}))
        .pipe(gulp.dest('./dist/css/'))
);
 

gulp.task('watch', function(){
		gulp.watch(['./app/*.jade'], ['html'] );
		gulp.watch(['./app/sass/**/*.scss'], ['css'] );

});
gulp.task('default', [ 'css', 'webserver', 'watch', 'html' ])
